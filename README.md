# mpv-script-mozbugbox

mpv script collection by mozbugbox

Copyright (c) 2015-2018 mozbugbox <mozbugbox@yahoo.com.au>  
Licensed under GPL version 3 or later

## General Usage
How to use a script?

  * Copy script file to ~/.config/mpv/scripts/
  * Add following lines to ~/.config/mpv/input.conf

    ```
    b osd-msg script-message curves-brighten-show
    y osd-msg script-message curves-cooler-show
    ```

  * Start mpv and press the binded keys: `b`, `y` ...

## Scripts

Here are the scripts.

### curvesman
Manipulate [color curve](https://en.wikipedia.org/wiki/Curve_(tonality)) by
using the curves filter of FFmpeg.

Once in a `curves mode`, user can use *arrow keys* to move the point coordinate
on the curve.

#### Files

  * curvesman.lua
  * curvedraw.lua

script commands:

  * curves-brighten-show: Switch on bright manipulate mode, use arrow keys
  * curves-cooler-show: Switch on temperature manipulate mode, use arrow keys

  * curves-brighten: Adjust brightness of video. Param: +/-1
  * curves-brighten-tone: Change the tone base (the x-coord) Param: +/-1
  * curves-temp-cooler: Adjust the temperature by change r,b,g curve values
  * curves-temp-tone: Change the tone base (the x-coord)

Usage:

  * Copy script files to ~/.config/mpv/scripts/
  * Add following lines to ~/.config/mpv/input.conf

    ```
    b osd-msg script-message curves-brighten-show
    y osd-msg script-message curves-cooler-show
    ```
  * In mpv, press `b`, `y` keys to start manipulating color curves
  * Use arrow keys to move the point in curve up/down/left/right
  * `r`: reset curves state
  * `d`: delete the filter
  * press `b`, `y` keys again to exist the curves modes.

### clock
Show a clock on the Video

Usage:
```
c script_message show-clock [true|yes]
```

### filter-test
Test mpv and FFmpeg filters lives.

Depends:
  * yad: Yad Another Dialog
  * mpv vf: https://github.com/mpv-player/mpv/blob/master/DOCS/man/vf.rst
  * FFmpeg filter: https://ffmpeg.org/ffmpeg-filters.html

Usage:
  * Bind a hotkey to the script command `filter-test`.
  * Wake up the script command with the hotkey and type in filter text.
  * Execute the filter text on the currently playing video.
  * An empty filter text remove the filter.
  * Applied filter will be print out in the command line.
